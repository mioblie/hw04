import 'dart:async';

abstract class Monster {
  var name;
  var elements;
  var type;

  Monster(name, elements, type) {
    this.name = name;
    this.elements = elements;
    this.type = type;
  }
}

class ABMon {
  void NameMon() {}
  void ElementsMon() {}
  void TypeMon() {}
}

class Sj extends Monster implements ABMon {
  Sj(super.name, super.elements, super.type);

  void NameMon() {
    print("Montser is name : $name");
  }

  void ElementsMon() {
    print("$name is element : $elements");
  }

  void TypeMon() {
    print("$name is type : $type");
  }
}

class Tk extends Monster implements ABMon {
  Tk(super.name, super.elements, super.type);

  void NameMon() {
    print("Montser is name : $name");
  }

  void ElementsMon() {
    print("$name is element : $elements");
  }

  void TypeMon() {
    print("$name is type : $type");
  }
}

class Rh extends Monster implements ABMon {
  Rh(super.name, super.elements, super.type);

  void NameMon() {
    print("Montser is name : $name");
  }

  void ElementsMon() {
    print("$name is element : $elements");
  }

  void TypeMon() {
    print("$name is type : $type");
  }
}

void main(List<String> args) {
  print("-------------------------------------");
  print("Welcome to Monster that i like in MonsterHunter");
  print("Number.01");
  var Safi = new Sj("Safijiiva", "None", "Elder Dragon");
  Safi.NameMon();
  Safi.ElementsMon();
  Safi.TypeMon();
  print("-------------------------------------");
  print("Number.02");
  var Ratha = new Rh("Rathalos", "Fire", "Flying Wyvern");
  Ratha.NameMon();
  Ratha.ElementsMon();
  Ratha.TypeMon();
  print("-------------------------------------");
  print("Number.03");
  var Tobi = new Tk("Tobi-Kadachi", "Thunder", "Fanged Wyvern");
  Tobi.NameMon();
  Tobi.ElementsMon();
  Tobi.TypeMon();
  print("-------------------------------------");
}
